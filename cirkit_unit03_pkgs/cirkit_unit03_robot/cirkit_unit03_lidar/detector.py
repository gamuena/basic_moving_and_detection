#!/usr/bin/env python
import rospy
from std_msgs.msg import String
import sensor_msgs.msg
import random
import numpy as np
from std_msgs.msg import Int32
from itertools import groupby

LINX = 0.2 #Always forward linear velocity.
THRESHOLD = 1.5 #THRESHOLD value for laser scan.

pub = None

def LaserScanProcess(data):
    global pub
    range_vals = np.arange(len(data.ranges))
    ranges = np.array(data.ranges)
    range_mask = np.logical_and((ranges > 0.5) , (ranges < THRESHOLD))
    front_mask = range_mask[170: -170]
    print(range_mask[170: -170])
    command = Int32()
    command.data = any(front_mask)
    print("Obstaculo", command) 
    pub.publish(command)
    

def main():
    global pub
    rospy.init_node('listener', anonymous=True)
    pub = rospy.Publisher('/lidar_obstacle_detector', Int32, queue_size=10)
    rospy.Subscriber("scan", sensor_msgs.msg.LaserScan , LaserScanProcess)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        rospy.spin()
        rate.sleep()

if __name__ == '__main__':
    main()
