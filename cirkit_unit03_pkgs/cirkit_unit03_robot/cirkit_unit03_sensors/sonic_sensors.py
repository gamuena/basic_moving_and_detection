#!/usr/bin/env python
# license removed for brevity

import rospy
import RPi.GPIO as GPIO
import time
from rospy.numpy_msg import numpy_msg
from std_msgs.msg import Int32
import os

GPIO.setmode (GPIO.BCM)

VERBOSITY = True

ERROR_IN_SETTINGS = False

TIMEOUT = 0.03

pulse_duration = 0

TRIG_0 = rospy.get_param('TRIG_0',17)
ECHO_0 = rospy.get_param('ECHO_0',27)

TRIG_1 = rospy.get_param('TRIG_1',23)
ECHO_1 = rospy.get_param('ECHO_1',24)

TRIG_2 = rospy.get_param('TRIG_2',5)
ECHO_2 = rospy.get_param('ECHO_2',6)

TRIG_3 = rospy.get_param('TRIG_3',12)
ECHO_3 = rospy.get_param('ECHO_3',16)

MIN_LENGHT = rospy.get_param('MIN_LENGHT',40.0)

TRIGGER = [TRIG_0, TRIG_1, TRIG_2, TRIG_3]
ECHO = [ECHO_0, ECHO_1, ECHO_2, ECHO_3]

GPIO.setwarnings(False)

measures_dict = [{'status': True, 'measure': 0.0, 'times_failed': 0, 'times_not_attended': 0} for x in range(4)]

warning_array = [0, 0, 0, 0]

def pinout_config():

    ERROR_IN_SETTINGS = True

    rospy.loginfo("Setting GPIO pins: START")
    for x in range(4):
        rospy.loginfo("\t-Setting #%d sensor pin trigger:",x)
        GPIO.setup(TRIGGER[x], GPIO.OUT)
        time.sleep(0.5)
        if VERBOSITY:
            if GPIO.gpio_function(TRIGGER[x]) == 0:
                rospy.loginfo("\t\t-PIN BCM #%d set correctly as output",TRIGGER[x])
            else:
                rospy.logwarn("\t\t-PIN BCM #%d not set correctly",TRIGGER[x])
                ERROR_IN_SETTINGS = True
    for x in range(4):
        rospy.loginfo("\t-Setting #%d sensor pin echo:",x)
        GPIO.setup(ECHO[x], GPIO.IN)
        time.sleep(0.5)
        if VERBOSITY:
            if GPIO.gpio_function(ECHO[x]) == 1:
                rospy.loginfo("\t\t-PIN BCM #%d set correctly as input", ECHO[x])
            else:
                rospy.logwarn("\t\t-PIN BCM #%d not set correctly", ECHO[x])
                ERROR_IN_SETTINGS = True
    if ERROR_IN_SETTINGS:
        rospy.logwarn("Setting GPIO pins: FINISHED WITH ERRORS")
    else:
        rospy.loginfo("Setting GPIO pins: FINISH")

def all_false():
    for x in range(4):
        GPIO.output(TRIGGER[x], False)

def measure():
    all_false()

    ERROR_IN_MEASUREMENT = False

    for x in range(4):
        if measures_dict[x]['times_failed'] > 100:
            rospy.logwarn("\t-Sensor #%d",x)
            rospy.logwarn("\t\t-Measure: FAIL (reason: Sensor unreacheable)")
            measures_dict[x]['times_not_attended'] +=1
            if measures_dict[x]['times_not_attended'] > 1000:
                rospy.logwarn("\t\t-Sensor unatended for enough time: retrying")
                measures_dict[x]['times_not_attended'] = 0
                measures_dict[x]['times_failed'] = 0
            break;
        else:
            rospy.loginfo("\t-Sensor #%d", x)
            GPIO.output(TRIGGER[x], True)
            time.sleep(0.0001)
            GPIO.output(TRIGGER[x], False)

            initial_pulse_start = time.time()

            pulse_start = time.time()
            pulse_end = time.time()

            initial_pulse_start = pulse_start

            while GPIO.input(ECHO[x])==0:
                pulse_start = time.time()
                if (pulse_start - initial_pulse_start) > TIMEOUT:
                    rospy.logwarn("\t\t-Measure: FAIL (reason: Timeout reached)")
                    ERROR_IN_MEASUREMENT = True
                    measures_dict[x]['status'] = False
                    measures_dict[x]['times_failed'] += 1
                    break;

            while GPIO.input(ECHO[x])==1:
                pulse_end = time.time()
                if (pulse_end - pulse_start) > TIMEOUT:
                    if (pulse_start - initial_pulse_start) < TIMEOUT:
                        rospy.logwarn("\t\t-Measure: FAIL (reason: Timeout reached)")
                        ERROR_IN_MEASUREMENT = True
                        measures_dict[x]['status'] = False
                        measures_dict[x]['times_failed'] += 1
                        break;

            if not ERROR_IN_MEASUREMENT:
                rospy.loginfo("\t\t-Measure: OK")
                measures_dict[x]['status'] = True

            pulse_duration = pulse_end - pulse_start
            distance = (pulse_duration * 34300) / 2
            measures_dict[x]['measure'] = distance


    rospy.loginfo("Obtained measurements:")

    for x in range(4):
        if measures_dict[x]['status']:
            rospy.loginfo("\t-Sensor #%d measured: %f cm", x, measures_dict[x]['measure'])

def warning_array_fill():
    for x in range(4):
        if measures_dict[x]['measure'] < MIN_LENGHT and measures_dict[x]['status']:
            warning_array[x] = (-1)*(x + 1)
        else:
            warning_array[x] = (x + 1)
    for x in range(4):
        rospy.loginfo("\t-Sensor #%d status: %d",x,warning_array[x])


def sensors():
    pub = rospy.Publisher('sensors', numpy_msg(Int32), queue_size=10)
    rospy.init_node('sensors', anonymous=True)
    rate = rospy.Rate(1)
    pinout_config()
    while not rospy.is_shutdown():
        _=os.system("clear")
        measure()
        warning_array_fill()
        pub.publish(warning_array)
        rate.sleep()

if __name__ == '__main__':
    try:
        sensors()
    except rospy.ROSInterruptException:
        pass
    except KeyboardInterrupt:
        pass
GPIO.cleanup()
