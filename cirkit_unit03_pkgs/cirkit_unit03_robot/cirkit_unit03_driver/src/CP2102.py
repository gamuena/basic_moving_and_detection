import serial
import time
import sys
import pyvesc
import pycrc

PORT_NAME_D = '/dev/ttyUSB0'
BAUDRATE_D  = 9600
BYTE_SIZE_D = serial.EIGHTBITS
PARITY_D    = serial.PARITY_NONE
STOPBITS_D  = serial.STOPBITS_ONE
TIME_OUT_D  = 1.0
XONXOFF_D   = False
RTSCTS_D    = False
DSRDTR_D    = False
WRITE_TO_D  = 2.0

class CP2102():

    def set_port(self):
        self.ser = serial.Serial()
        self.ser.port = PORT_NAME_D
        self.ser.baudrate = BAUDRATE_D
        self.ser.bytesize = BYTE_SIZE_D
        self.ser.parity = PARITY_D
        self.ser.stopbits = STOPBITS_D
        self.ser.timeout = TIME_OUT_D
        self.ser.xonxoff = XONXOFF_D
        self.ser.rtscts = RTSCTS_D
        self.ser.dsrdtr = DSRDTR_D
        self.ser.writeTimeout = WRITE_TO_D
        self.ser.close()


    def set_port(self, port, baudrate, bytesize, parity, stopbits):
        self.ser = serial.Serial()
        self.ser.port = port
        self.ser.baudrate = baudrate
        self.ser.bytesize = bytesize
        self.ser.parity = parity
        self.ser.stopbits = stopbits
        self.ser.timeout = TIME_OUT_D
        self.ser.xonxoff = XONXOFF_D
        self.ser.rtscts = RTSCTS_D
        self.ser.dsrdtr = DSRDTR_D
        self.ser.writeTimeout = WRITE_TO_D
        self.ser.close()

    def open_port(self):
        if self.ser.isOpen():
            self.ser.close()
        self.ser.open()

    def write_str (self, message):
        message_as_bytes = message.encode('utf-8')
        bw = self.ser.write(message_as_bytes)
        return bw

    def read_data (self, bytes_to_read):
        read_bytes = self.ser.read(bytes_to_read)
        str_read = read_bytes.decode('utf-8')
        return str_read

    def close_port(self):
        if self.ser.isOpen():
            self.ser.close()

    def write_RPM (self, RPM):
        if not self.ser.isOpen():
            self.ser.open()
        payload = pyvesc.SetRPM(RPM)
        package = pyvesc.encode(payload)
        self.ser.write(package)

    def write_DutyCycle(self, Duty):
        if not self.ser.isOpen():
            self.ser.open()
        payload = pyvesc.SetDutyCycle(Duty)
        package = pyvesc.encode(payload)
        self.ser.write(package)

    def write_Current (self, current):
        if not self.ser.isOpen():
            self.ser.open()
        payload = pyvesc.SetCurrent(current)
        package = pyvesc.encode(payload)
        self.ser.write(package)

    def write_CurrentBrake (self, current):
        if not self.ser.isOpen():
            self.ser.open()
        payload = pyvesc.SetCurrentBrake(current)
        package = pyvesc.encode(payload)
        self.ser.write(package)

    def read_RPM (self):
        if not self.ser.isOpen():
            self.ser.open()
        self.ser.write(pyvesc.encode_request(pyvesc.GetValues))
        if self.ser.in_waiting > 61:
            (response,consumed) = pyvesc.decode(self.ser.read(61))
#            print(response.rpm)

    def flush (self):
        if not self.ser.isOpen():
            self.ser.open()
        self.ser.flush()
