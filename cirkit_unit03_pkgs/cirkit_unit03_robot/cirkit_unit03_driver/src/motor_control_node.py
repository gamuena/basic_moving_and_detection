#!/usr/bin/python3

import rospy
import serial
import sys
import os
import time
from CP2102 import CP2102
from std_msgs.msg import Int32, Float64

Motor_L = CP2102()
Motor_R = CP2102()

PORT_0 = '/dev/ttyUSB0'
PORT_1 = '/dev/ttyUSB1'

PUBLISHER_NAME = "motor_velocity"

DIRECTION_1 = 1
DIRECTION_2 = 2
DIRECTION_3 = 3
DIRECTION_4 = 4
DIRECTION = DIRECTION_1

PI = 3.141592653589793
RADIUS = 0.2

rpm_value = 0
vel_value = 0
TIMEOUT = 3


def RPM_to_mps (rpm_value):
    linear_vel = ((2*PI*RADIUS)/(60))*rpm_value
    return linear_vel

def mps_to_RPM (linear_vel):
    rpm_val = ((60)/(2*PI*RADIUS))*linear_vel
    return rpm_val

def init():
    Motor_L.set_port(PORT_0, 115200, serial.EIGHTBITS, serial.PARITY_NONE
                     ,serial.STOPBITS_ONE)
    Motor_R.set_port(PORT_1, 115200, serial.EIGHTBITS, serial.PARITY_NONE
                     ,serial.STOPBITS_ONE)
    Motor_L.open_port()
    Motor_R.open_port()

def keep_alive():
    if vel_value == 0:
        Motor_L.write_CurrentBrake(1000)
        Motor_R.write_CurrentBrake(1000)
    else:
        if DIRECTION == DIRECTION_1:
            Motor_L.write_RPM(rpm_value)
            Motor_R.write_RPM(rpm_value)
        elif DIRECTION == DIRECTION_2:
            Motor_L.write_RPM(-rpm_value)
            Motor_R.write_RPM(rpm_value)
        elif DIRECTION == DIRECTION_3:
            Motor_L.write_RPM(rpm_value)
            Motor_R.write_RPM(-rpm_value)
        elif DIRECTION == DIRECTION_4:
            Motor_L.write_RPM(-rpm_value)
            Motor_R.write_RPM(-rpm_value)

def set_velocity(vel_in_mps):
    if vel_in_mps == 0:
        Motor_L.write_CurrentBrake(1000)
        Motor_R.write_CurrentBrake(1000)
    else:
        vel_value = vel_in_mps
        rpm_value = mps_to_RPM(vel_value)
        if DIRECTION == DIRECTION_1:
            Motor_L.write_RPM(rpm_value)
            Motor_R.write_RPM(rpm_value)
        elif DIRECTION == DIRECTION_2:
            Motor_L.write_RPM(-rpm_value)
            Motor_R.write_RPM(rpm_value)
        elif DIRECTION == DIRECTION_3:
            Motor_L.write_RPM(rpm_value)
            Motor_R.write_RPM(-rpm_value)
        elif DIRECTION == DIRECTION_4:
            Motor_L.write_RPM(-rpm_value)
            Motor_R.write_RPM(-rpm_value)


def hw_driver():
    rospy.init_node('motor_control_node', anonymous=True)
    rospy.Subscriber(PUBLISHER_NAME, Float64, set_velocity)

if __name__ == '__main__':
    hw_driver()
    while (not time.sleep(TIMEOUT)):
        keep_alive()

else:
    Motor_L.write_CurrentBrake(1000)
    Motor_R.write_CurrentBrake(1000)
    Motor_L.close_port()
    Motor_R.close_port()
