import serial
import sys
import os
import time
from CP2102 import CP2102

Motor_L = CP2102()
Motor_R = CP2102()

PORT_0 = '/dev/ttyUSB0'
PORT_1 = '/dev/ttyUSB1'

DIRECTION_1 = 1
DIRECTION_2 = 2
DIRECTION_3 = 3
DIRECTION_4 = 4
DIRECTION = DIRECTION_1

Motor_L.set_port(PORT_0, 115200, serial.EIGHTBITS, serial.PARITY_NONE
                 ,serial.STOPBITS_ONE)

Motor_R.set_port(PORT_1, 115200, serial.EIGHTBITS, serial.PARITY_NONE
                 ,serial.STOPBITS_ONE)

os.system('clear')

Motor_L.open_port()
Motor_R.open_port()

input("Ports Open Sucessfully! Press Enter to continue")

os.system('clear')

print ("Sending first command (RPM forward) for 5 seconds:")

t_end = time.time() + 5

while time.time() < t_end:
    if DIRECTION == DIRECTION_1:
        Motor_L.write_RPM(5000)
        Motor_R.write_RPM(5000)
        Motor_L.read_RPM()
        Motor_R.read_RPM()
    elif DIRECTION == DIRECTION_2:
        Motor_L.write_RPM(-5000)
        Motor_R.write_RPM(5000)
        Motor_L.read_RPM()
        Motor_R.read_RPM()
    elif DIRECTION == DIRECTION_3:
        Motor_L.write_RPM(5000)
        Motor_R.write_RPM(-5000)
        Motor_L.read_RPM()
        Motor_R.read_RPM()
    elif DIRECTION == DIRECTION_4:
        Motor_L.write_RPM(-5000)
        Motor_R.write_RPM(-5000)
        Motor_L.read_RPM()
        Motor_R.read_RPM()

Motor_L.write_CurrentBrake(1000)
Motor_R.write_CurrentBrake(1000)

print ("Sending second command (RPM backward) for 5 seconds:")

t_end = time.time() + 5

while time.time() < t_end:
    if DIRECTION == DIRECTION_1:
        Motor_L.write_RPM(-5000)
        Motor_R.write_RPM(-5000)
        Motor_L.read_RPM()
        Motor_R.read_RPM()
    elif DIRECTION == DIRECTION_2:
        Motor_L.write_RPM(5000)
        Motor_R.write_RPM(-5000)
        Motor_L.read_RPM()
        Motor_R.read_RPM()
    elif DIRECTION == DIRECTION_3:
        Motor_L.write_RPM(-5000)
        Motor_R.write_RPM(5000)
        Motor_L.read_RPM()
        Motor_R.read_RPM()
    elif DIRECTION == DIRECTION_4:
        Motor_L.write_RPM(5000)
        Motor_R.write_RPM(5000)
        Motor_L.read_RPM()
        Motor_R.read_RPM()

Motor_L.write_CurrentBrake(1000)
Motor_R.write_CurrentBrake(1000)

print ("Test Finished: Exiting")

Motor_L.close_port()
Motor_R.close_port()
